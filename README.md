### APULIA URBAN LABORATORIES

Utilizza dati disponibili su dataset di Open Data Puglia per mostrare
in una comoda interfaccia laboratori urbani per giovani nel territorio
della Regione.

Possibilità di visualizzare una piccola mappa e di aprirla in Mappe di Windows
    (disponibile solo su Windows 10)

Realizzato in WPF. Utilizza la libreria Newtonsoft.JSON.

# NB.
Per l'utilizzo della mappa è necessario avere una chiave API per Bing Mappe
facilmente ottenibile su *https://www.bingmapsportal.com/* .
Copiare la chiave ottenuta in *OpenDataApulia_UrbanLabs.MapUtils.BING_KEY*.

### LICENSED UNDER THE MIT-EXPAT LICENSE.

Copyright (c) 2017 thyoften.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.