﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenDataApulia_UrbanLabs
{
    /// <summary>
    /// Astrazione di un dato filtrato dal documento XML
    /// </summary>
    public class LaboratorioUrbano
    {
        public string Localita { get; set; }
        public string Provincia { get; set; }
        public string NomeLaboratorio { get; set; }
        public string IndirizzoLaboratorio { get; set; }
        public string Immobile { get; set; }
        public string CantiereConcluso { get; set; }
        public string NomeGestore { get; set; }
        public string DataAvvioGestione { get; set; }
        public string DurataContratto { get; set; }
        public string DataScadenzaGestione { get; set; }
        public string Stato { get; set; }
        public string StartUpPrimoAnnoInCorso { get; set; }
    }
}
