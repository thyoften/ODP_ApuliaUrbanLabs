﻿using Microsoft.Maps.MapControl.WPF;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Newtonsoft.Json;
using System.IO;
using Newtonsoft.Json.Linq;

namespace OpenDataApulia_UrbanLabs
{ 

    public static class MapUtils
    {
        const string BING_KEY = "INSERT-YOUR-BING-KEY-HERE";

        public static Location RicercaBingMaps(string query)
        {
            string url = "http://dev.virtualearth.net/REST/v1/Locations?q=" + query + "&key=" + BING_KEY;

            string jsonRaw = (new WebClient()).DownloadString(url);

            //MessageBox.Show(jsonRaw);

            var json = JObject.Parse(jsonRaw);

            var lat = (double)json.SelectToken("resourceSets[0].resources[0].point.coordinates[0]");
            var longd = (double)json.SelectToken("resourceSets[0].resources[0].point.coordinates[1]");

            return new Location { Latitude =  lat  /* lat */, Longitude = longd /* longd */ };
        }

    }
}
