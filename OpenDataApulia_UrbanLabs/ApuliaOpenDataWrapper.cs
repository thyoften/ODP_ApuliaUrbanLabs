﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Xml;

namespace OpenDataApulia_UrbanLabs
{
    /// <summary>
    /// Gestione dei dati XML (aggiornamento, filtraggio) ottenututi dal dataset della Regione
    /// </summary>
    public class ApuliaOpenDataWrapper
    {
        const string URL_BASE = "http://www.dataset.puglia.it/dataset/9ddb83aa-16a2-4a34-88eb-674750bbbf91/resource/e6fd8182-d316-40f0-834b-9da016911d43/download";

        private List<LaboratorioUrbano> laboratori;

        public ApuliaOpenDataWrapper()
        {
            laboratori = new List<LaboratorioUrbano>();
        }

        /// <summary>
        /// Scarica una nuova copia dei dati e aggiorna la lista dei laboratori
        /// </summary>
        public void AggiornaDati()
        {
            laboratori.Clear(); //Cancello i vecchi dati

            //Scarico la stringa XML e la carico in un XmlDocument per inizializzare la List
            string rawXml = (new WebClient()).DownloadString(URL_BASE);
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(rawXml);

            var labsRaw = xml.SelectNodes("/root/row");
            foreach(XmlNode nodo in labsRaw)
            {
                LaboratorioUrbano lab = new LaboratorioUrbano();
                lab.Localita = nodo["Localita"].InnerText;
                lab.Provincia = nodo["Provincia"].InnerText;
                lab.NomeLaboratorio = nodo["Nome_Laboratorio"].InnerText;
                lab.IndirizzoLaboratorio = nodo["Indirizzo_Laboratorio"].InnerText;
                lab.Immobile = nodo["Immobile"].InnerText;
                lab.CantiereConcluso = nodo["Cantiere_conluso_non_Concluso"].InnerText;
                lab.NomeGestore = nodo["Nome_gestore_o_ex-gestore"].InnerText;
                lab.DataAvvioGestione = nodo["Data_avvio_gestione"].InnerText;
                lab.DurataContratto = nodo["Durata_Contratto"].InnerText;
                lab.DataScadenzaGestione = nodo["Data_scadenza_gestione"].InnerText;
                lab.Stato = nodo["Stato"].InnerText;
                lab.StartUpPrimoAnnoInCorso = nodo["Start_up_primo_anno_in_corso"].InnerText;

                laboratori.Add(lab);
            }
        }

        public List<LaboratorioUrbano> GetLaboratori()
        {
            return laboratori;
        }

        public List<LaboratorioUrbano> GetLaboratoriPerProvincia(string provincia)
        {
            return laboratori.FindAll(x => x.Provincia == provincia).ToList();
        }
    }
}
