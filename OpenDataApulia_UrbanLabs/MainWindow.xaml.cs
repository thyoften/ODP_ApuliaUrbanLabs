﻿using Microsoft.Maps.MapControl.WPF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OpenDataApulia_UrbanLabs
{
    /// <summary>
    /// Logica di interazione per MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ApuliaOpenDataWrapper wrapper;
        List<LaboratorioUrbano> risultati = new List<LaboratorioUrbano>();
        int index = 0;

        public MainWindow()
        {
            InitializeComponent();
            wrapper = new ApuliaOpenDataWrapper();
            wrapper.AggiornaDati(); //Primo caricamento
        }

        void AggiornaLabelMappa()
        {
            //Aggiorna label
            var corrente = risultati[index];
            lblNomeLab.Content = corrente.NomeLaboratorio;
            lblWhoAndWhere.Content = String.Format("di {0}\na {1} ({2})\nin {3}", corrente.NomeGestore, corrente.Localita, corrente.Provincia, corrente.IndirizzoLaboratorio);

            //Aggiorna mappa
            var indirizzo = corrente.IndirizzoLaboratorio.Replace(' ', '+') + "," + corrente.Localita.Replace(' ', '+');
            mappaLab.Center = MapUtils.RicercaBingMaps(indirizzo);
            mappaLab.ZoomLevel = 18;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //lstRisultati.Items.Clear();
            risultati.Clear();
            string prov = (comboBoxProv.SelectedItem as ComboBoxItem).Content.ToString();
            risultati = wrapper.GetLaboratoriPerProvincia(prov);
            lblQtaRis.Content = "Trovati " + risultati.Count + " laboratori.";
            //foreach (LaboratorioUrbano lab in risultati)
            //{
            //    string item = String.Format("{0} di {1} a {2} in {3} ({4})", lab.NomeLaboratorio, lab.NomeGestore, lab.Localita, lab.IndirizzoLaboratorio, lab.Provincia);
            //    lstRisultati.Items.Add(item);
            //}

            if(risultati.Count > 0)
            {
                AggiornaLabelMappa();
            }

        }

        private void btnTest_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void btnPrevLab_Click(object sender, RoutedEventArgs e)
        {
            if(index > 0)
            {
                index--;
            } else
            {
                index = risultati.Count - 1;
            }
            AggiornaLabelMappa();

        }

        private void btnNextLab_Click(object sender, RoutedEventArgs e)
        {
            if(index < risultati.Count - 1)
            {
                index++;
            } else
            {
                index = 0;
            }
            AggiornaLabelMappa();
        }

        private void mainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            risultati.Clear();
            string prov = (comboBoxProv.SelectedItem as ComboBoxItem).Content.ToString();
            risultati = wrapper.GetLaboratoriPerProvincia(prov);
            lblQtaRis.Content = "Trovati " + risultati.Count + " laboratori.";// per ricerca su '" + prov + "'";
            //foreach (LaboratorioUrbano lab in risultati)
            //{
            //    string item = String.Format("{0} di {1} a {2} in {3} ({4})", lab.NomeLaboratorio, lab.NomeGestore, lab.Localita, lab.IndirizzoLaboratorio, lab.Provincia);
            //    lstRisultati.Items.Add(item);
            //}

            if (risultati.Count > 0)
            {
                AggiornaLabelMappa();
            }
        }

        private void btnOpenWinMaps_Click(object sender, RoutedEventArgs e)
        {
            //var osv = System.Environment.OSVersion;
            //var os2 = System.Environment.Version.ToString();
            //MessageBox.Show(os2);

            var osinfo = new Microsoft.VisualBasic.Devices.ComputerInfo().OSFullName;

            if (osinfo.Contains("Windows 10"))
            {
                var corrente = risultati[index];
                var indirizzo = corrente.IndirizzoLaboratorio.Replace(' ', '+') + "," + corrente.Localita.Replace(' ', '+');
                System.Diagnostics.Process.Start("bingmaps:?q=" + indirizzo);
            }
            else
            {
                MessageBox.Show("La funzionalità richiesta non è disponibile sul sistema operativo in uso.", "Errore versione Windows", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }
    }
}
